import dev from "./config.dev.json"
import test from "./config.test.json"
import prod from "./config.prod.json"

export enum Environment {
  DEV = 'development',
  TEST = 'test',
  PROD = 'production'
}

export default (env: Environment) => {
  switch (env) {
    case Environment.DEV:
      process.env = { ...process.env, ...dev }

    case Environment.TEST:
      process.env = { ...process.env, ...test }

    case Environment.PROD:
      process.env = { ...process.env, ...prod }
  }
}

