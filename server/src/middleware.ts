import jwt from 'express-jwt'
import jwks from 'jwks-rsa'

export const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: process.env.jwksUri!
  }),
  audience: process.env.aud,
  issuer: process.env.iss,
  algorithms: ['RS256']
});