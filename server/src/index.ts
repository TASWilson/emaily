import express, { Request, Response } from 'express';
import config, { Environment } from './config'
import { jwtCheck } from './middleware'

// Add environment config
config(process.env.NODE_ENV as Environment)

const app = express();

app.use(jwtCheck)

app.get('/', (req: Request, res: Response) => {
  res.send({ hi: 'there' })
})

const port = process.env.PORT || 3002
app.listen(port);